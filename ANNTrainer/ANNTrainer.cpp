#include <iostream>
#include <ANN.h>
#include<vector>
using namespace std;
using namespace ANN;

int main()
{
	vector<vector<float>>inputs; //������� ������ ������� ������
	vector<vector<float>>outputs; //������� ������ �������� ������
	//���� � ���������� ������� ��������� �� ������:
	string address = "data.txt";
	LoadData(address, inputs, outputs); //������ ��������� ������ �� �����

	//������� ��������� ���� ������������ ������������	
	vector<int>config;
	config.resize(4);
	config[0] = 2;
	config[1] = 10;
	config[2] = 10;
	config[3] = 1;
	auto ann = CreateNeuralNetwork(config, ANeuralNetwork::POSITIVE_SYGMOID	);

	//������� ��������� ����
	BackPropTrain(ann, inputs, outputs, 100000, 1.e-2, 0.1, true); //��� �������� �� xor ������������� ������������ 
	                                                        //��������� �� ��������� (eps=0.1, max_iter=100000, speed=0.1)
	//��������� ��������� ��������� ���� � ��������� ����
	ann->Save("ann.ann");
}