#include <iostream>
#include <ANN.h>
using namespace std;
using namespace ANN;

int main()
{
	cout << "hello my dear friends!" << endl;
	cout << GetTestString().c_str() << endl;
	auto net = CreateNeuralNetwork();
	if (!net->Load("..//network.dat"))
	{
		cout << "Can't load net" << endl;
	}
	else
	{
		net->GetType();
		vector<vector<float>>inputData;
		vector<vector<float>>outputData;
		
		if (!LoadData("..//data.dat", inputData, outputData))
		{
			cout << "Can't load data network" << endl;
		}
		for (int i = 0; i < inputData.size(); i++)
		{
			auto a = net->Predict(inputData[i]);
			cout << endl;
			cout << "input data: " << inputData[i][0] << "\t" << inputData[i][1] << endl;
			cout << "expected data: " << outputData[i][0] << endl;
			cout << "output: " << a[0] << endl;
		}
	}
	system("pause");
	return 0;
}